# import asyncio
# from bleak import BleakScanner, BleakClient

# async def main():
#     while 1:
#         devices = await BleakScanner.discover()
#         for d in devices:
#             if d.name == "ESP_SPP_SERVER":
#                 print(d.address)
#                 async with BleakClient(d.address) as client:
#                     model_number = await client.read_gatt_char("0xABF0")
#                     print("Model Number: {0}".format("".join(map(chr, model_number))))

# asyncio.run(main())
"""
Service Explorer
----------------

An example showing how to access and print out the services, characteristics and
descriptors of a connected GATT server.

Created on 2019-03-25 by hbldh <henrik.blidh@nedomkull.com>

"""

import argparse
import asyncio
import logging

from bleak import BleakClient, BleakScanner

logger = logging.getLogger(__name__)

def bleCallback(char, data):
    # print(data.hex())
    idbyte = data[0:4]
    print(hex(int.from_bytes(idbyte,byteorder="little")))
    print(data[4:])
    # print(data.decode())
    # print(str(data,'UTF-8'))
    # print(data.decode('utf-8'))
    # print(char)
async def main(args: argparse.Namespace):
    logger.info("starting scan...")
    print(args)
    if args.address:
        device = await BleakScanner.find_device_by_address(
            args.address, cb=dict(use_bdaddr=args.macos_use_bdaddr)
        )
        if device is None:
            logger.error("could not find device with address '%s'", args.address)
            return
    else:
        device = await BleakScanner.find_device_by_name(
            args.name, cb=dict(use_bdaddr=args.macos_use_bdaddr)
        )
        if device is None:
            logger.error("could not find device with name '%s'", args.name)
            return

    logger.info("connecting to device...")

    async with BleakClient(
        device,
        services=args.services,
    ) as client:
        logger.info("connected")

        for service in client.services:
            logger.info("[Service] %s", service)
            print(service.uuid)
            if service.uuid == "0000abf0-0000-1000-8000-00805f9b34fb":
                for char in service.characteristics:
                    if char.uuid == "0000abf2-0000-1000-8000-00805f9b34fb":
                        await client.start_notify(char, bleCallback)
                        try:
                            while 1:
                                value = await client.read_gatt_char(char.uuid)
                                # logger.info(
                                #     "  [Characteristic] %s, Value: %r",
                                #     char.uuid,
                                #     # ",".join(char.properties),
                                #     value.hex(),
                                # )
                        except Exception as e:
                            logger.error(
                                "  [Characteristic] %s (%s), Error: %s",
                                char,
                                ",".join(char.properties),
                                e,
                            )
                            break

                # else:
                #     logger.info(
                #         "  [Characteristic] %s (%s)", char, ",".join(char.properties)
                #     )

                # for descriptor in char.descriptors:
                #     try:
                #         value = await client.read_gatt_descriptor(descriptor.handle)
                #         logger.info("    [Descriptor] %s, Value: %r", descriptor, value)
                #     except Exception as e:
                #         logger.error("    [Descriptor] %s, Error: %s", descriptor, e)
                while 1:
                    i=1
        logger.info("disconnecting...")

    logger.info("disconnected")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    device_group = parser.add_mutually_exclusive_group(required=True)

    device_group.add_argument(
        "--name",
        metavar="<name>",
        help="the name of the bluetooth device to connect to",
    )
    device_group.add_argument(
        "--address",
        metavar="<address>",
        help="the address of the bluetooth device to connect to",
    )

    parser.add_argument(
        "--macos-use-bdaddr",
        action="store_true",
        help="when true use Bluetooth address instead of UUID on macOS",
    )

    parser.add_argument(
        "--services",
        nargs="+",
        metavar="<uuid>",
        help="if provided, only enumerate matching service(s)",
    )

    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="sets the log level to debug",
    )

    args = parser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(
        level=log_level,
        format="%(asctime)-15s %(name)-8s %(levelname)s: %(message)s",
    )

    asyncio.run(main(args))