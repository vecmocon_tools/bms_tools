Run the exe file on terminal with following flags
The script assumes default CAN_USB with:
	PID = 0xA455
	VID = 0x042D

To use custom CAN_USB use --c flag

--r=1 		(Reads complete eeprom and logs in a file)
--ev=1 		(Erases voltage mismatch flag)
--et=1		(Erases thermal runaway flag)
--c=COM45 	(Connects to CAN_USB at COM45)

example 1:
eepromRead.exe --r=1

example 2:
eepromRead.exe --c COM34 --r=1 --ev=1

example 3:
eepromRead.exe --et=1 --ev=1
